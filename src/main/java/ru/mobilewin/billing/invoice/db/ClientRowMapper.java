//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package ru.mobilewin.billing.invoice.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ru.mobilewin.billing.invoice.model.Client;

public class ClientRowMapper implements RowMapper<Client> {
    public ClientRowMapper() {
    }

    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Client(rs.getString("inn"), rs.getString("trrc"), rs.getString("name"), rs.getString("cust_addr"), rs.getString("gos_contract"), rs.getString("cjt_id"), rs.getString("account"));
    }
}
