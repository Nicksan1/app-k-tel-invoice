package ru.mobilewin.billing.invoice.model;

public class Company {
    private String companyName;
    private String rs;
    private String bankName;
    private String bik;
    private String ks;
    private String jurAddress;
    private String inn;
    private String trrc;
    private String buhgalter;
    private String director;
    private String phone;
    private String fax;

    public Company(String companyName, String rs, String bankName, String bik, String ks, String jurAddress, String inn, String trrc, String buhgalter, String director, String phone, String fax) {
        this.companyName = companyName;
        this.rs = rs;
        this.bankName = bankName;
        this.bik = bik;
        this.ks = ks;
        this.jurAddress = jurAddress;
        this.inn = inn;
        this.trrc = trrc;
        this.buhgalter = buhgalter;
        this.director = director;
        this.phone = phone;
        this.fax = fax;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRs() {
        return this.rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getBankName() {
        return this.bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBik() {
        return this.bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getKs() {
        return this.ks;
    }

    public void setKs(String ks) {
        this.ks = ks;
    }

    public String getJurAddress() {
        return this.jurAddress;
    }

    public void setJurAddress(String jurAddress) {
        this.jurAddress = jurAddress;
    }

    public String getInn() {
        return this.inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getTrrc() {
        return this.trrc;
    }

    public void setTrrc(String trrc) {
        this.trrc = trrc;
    }

    public String getBuhgalter() {
        return this.buhgalter;
    }

    public void setBuhgalter(String buhgalter) {
        this.buhgalter = buhgalter;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
