package ru.mobilewin.billing.invoice;

import javax.sql.DataSource;
import oracle.jdbc.driver.OracleDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@ComponentScan
public class AppConfig {
    @Value("${username}")
    private String user/* = "B2BUSER"*/;
    @Value("${password}")
    private String pwd/* = "qWErty2021"*/;

    public AppConfig() {
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(OracleDriver.class.getName());
        ds.setUrl("jdbc:oracle:thin:@10.54.193.60:1521:wmdb");
        ds.setUsername(this.user);
        ds.setPassword(this.pwd);
        return ds;
    }

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}
