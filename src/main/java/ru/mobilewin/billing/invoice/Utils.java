package ru.mobilewin.billing.invoice;

import javax.xml.transform.Transformer;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilderFactory;

public class Utils
{
    public static String TIME_FORMAT;
    public static String DATE_FORMAT;

    public static void createXML() {
        final DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentFactory.newDocumentBuilder();
            final Document document = documentBuilder.newDocument();
            final Element root = document.createElement("\u0420¤\u0420°\u0420\u2116\u0420»");
            root.setAttribute("\u0420\u2019\u0420µ\u0421\u0402\u0421\u0403\u0420\u045f\u0421\u0402\u0420\u0455\u0420\u0456", "\u0420\u040e\u0420±\u0420\u0451\u0421\u04033");
            root.setAttribute("\u0420\u2019\u0420µ\u0421\u0402\u0421\u0403\u0420¤\u0420\u0455\u0421\u0402\u0420\u0458", "5.01");
            root.setAttribute("\u0420\ufffd\u0420\u0491\u0420¤\u0420°\u0420\u2116\u0420»", "ON_NSCHFDOPPR_2BE1bafca86a97011e1aec15cf3fc3369f0_2BE96282c64a96f11e18e0d5cf3fc3369f0_20160715_615d4da7-6f90-8c35-3cf0-5afb03dcdba8");
            document.appendChild(root);
            final Element element01 = document.createElement("\u0420\u040e\u0420\u0406\u0420\u0408\u0421\u2021\u0420\u201d\u0420\u0455\u0420\u0454\u0420\u045b\u0420±\u0420\u0455\u0421\u0402");
            element01.setAttribute("\u0420\ufffd\u0420\u0491\u0420\u045b\u0421\u201a\u0420\u0457\u0421\u0402", "2BE96282c64a96f11e18e0d5cf3fc3369f0");
            element01.setAttribute("\u0420\ufffd\u0420\u0491\u0420\u045f\u0420\u0455\u0420»", "2BE1bafca86a97011e1aec15cf3fc3369f0");
            root.appendChild(element01);
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource domSource = new DOMSource(document);
            final StreamResult streamResult = new StreamResult(new File("example.xml"));
            transformer.transform(domSource, streamResult);
            System.out.println("Done creating XML File");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        Utils.TIME_FORMAT = "HH.mm.ss";
        Utils.DATE_FORMAT = "dd.MM.yyyy";
    }
}
