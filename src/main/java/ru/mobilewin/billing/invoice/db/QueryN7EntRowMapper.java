package ru.mobilewin.billing.invoice.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import ru.mobilewin.billing.invoice.model.QueryN7Ent;

public class QueryN7EntRowMapper implements RowMapper<QueryN7Ent> {
    public QueryN7EntRowMapper() {
    }

    public QueryN7Ent mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new QueryN7Ent(rs.getString("fact_num"), rs.getString("fact_date"), rs.getLong("fact_id"), rs.getLong("clnt_id"), rs.getLong("itg_clnt_id"), rs.getLong("cprm_id"), rs.getLong("inv_id"), rs.getString("sdate"), rs.getString("edate"), rs.getString("det_name"), rs.getString("corr_num"), rs.getString("corr_date"), rs.getString("pay_docs"), rs.getString("tax_rate"), rs.getDouble("summ"), rs.getDouble("vat"), rs.getDouble("wovat"));
    }
}
