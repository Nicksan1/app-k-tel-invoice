package ru.mobilewin.billing.invoice.model;

public class QueryN7Ent {
    private String factNum;
    private String factDate;
    private Long factId;
    private Long clntId;
    private Long itgClntId;
    private Long cprmId;
    private Long invId;
    private String sDate;
    private String eDate;
    private String detName;
    private String corrNum;
    private String corrDate;
    private String payDocs;
    private String taxRate;
    private Double summ;
    private Double vat;
    private Double wovat;

    public QueryN7Ent(String factNum, String factDate, Long factId, Long clntId, Long itgClntId, Long cprmId, Long invId, String sDate, String eDate, String detName, String corrNum, String corrDate, String payDocs, String taxRate, Double summ, Double vat, Double wovat) {
        this.factNum = factNum;
        this.factDate = factDate;
        this.factId = factId;
        this.clntId = clntId;
        this.itgClntId = itgClntId;
        this.cprmId = cprmId;
        this.invId = invId;
        this.sDate = sDate;
        this.eDate = eDate;
        this.detName = detName;
        this.corrNum = corrNum;
        this.corrDate = corrDate;
        this.payDocs = payDocs;
        this.taxRate = taxRate;
        this.summ = summ;
        this.vat = vat;
        this.wovat = wovat;
    }

    public String getFactNum() {
        return this.factNum;
    }

    public void setFactNum(String factNum) {
        this.factNum = factNum;
    }

    public String getFactDate() {
        return this.factDate;
    }

    public void setFactDate(String factDate) {
        this.factDate = factDate;
    }

    public Long getFactId() {
        return this.factId;
    }

    public void setFactId(Long factId) {
        this.factId = factId;
    }

    public Long getClntId() {
        return this.clntId;
    }

    public void setClntId(Long clntId) {
        this.clntId = clntId;
    }

    public Long getItgClntId() {
        return this.itgClntId;
    }

    public void setItgClntId(Long itgClntId) {
        this.itgClntId = itgClntId;
    }

    public Long getCprmId() {
        return this.cprmId;
    }

    public void setCprmId(Long cprmId) {
        this.cprmId = cprmId;
    }

    public Long getInvId() {
        return this.invId;
    }

    public void setInvId(Long invId) {
        this.invId = invId;
    }

    public String getsDate() {
        return this.sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String geteDate() {
        return this.eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }

    public String getDetName() {
        return this.detName;
    }

    public void setDetName(String detName) {
        this.detName = detName;
    }

    public String getCorrNum() {
        return this.corrNum;
    }

    public void setCorrNum(String corrNum) {
        this.corrNum = corrNum;
    }

    public String getCorrDate() {
        return this.corrDate;
    }

    public void setCorrDate(String corrDate) {
        this.corrDate = corrDate;
    }

    public String getPayDocs() {
        return this.payDocs;
    }

    public void setPayDocs(String payDocs) {
        this.payDocs = payDocs;
    }

    public String getTaxRate() {
        return this.taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public Double getSumm() {
        return this.summ;
    }

    public void setSumm(Double summ) {
        this.summ = summ;
    }

    public Double getVat() {
        return this.vat;
    }

    public void setVat(Double vat) {
        this.vat = vat;
    }

    public Double getWovat() {
        return this.wovat;
    }

    public void setWovat(Double wovat) {
        this.wovat = wovat;
    }
}
