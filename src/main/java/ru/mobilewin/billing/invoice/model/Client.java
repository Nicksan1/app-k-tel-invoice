package ru.mobilewin.billing.invoice.model;

public class Client {
    private String inn;
    private String trrc;
    private String name;
    private String custAddr;
    private String gosContract;
    private String cjtId;
    private String account;

    public Client(String inn, String trrc, String name, String custAddr, String gosContract, String cjtId, String account) {
        this.inn = inn;
        this.trrc = trrc;
        this.name = name;
        this.custAddr = custAddr;
        this.gosContract = gosContract;
        this.cjtId = cjtId;
        this.account = account;
    }

    public String getInn() {
        return this.inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getTrrc() {
        return this.trrc;
    }

    public void setTrrc(String trrc) {
        this.trrc = trrc;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustAddr() {
        return this.custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public String getGosContract() {
        return this.gosContract;
    }

    public void setGosContract(String gosContract) {
        this.gosContract = gosContract;
    }

    public String getCjtId() {
        return this.cjtId;
    }

    public void setCjtId(String cjtId) {
        this.cjtId = cjtId;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
