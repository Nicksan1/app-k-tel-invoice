package ru.mobilewin.billing.invoice;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.mobilewin.billing.invoice.db.Dao;
import ru.mobilewin.billing.invoice.model.Client;
import ru.mobilewin.billing.invoice.model.Company;
import ru.mobilewin.billing.invoice.model.QueryN7Ent;

@SpringBootApplication
public class Exporter implements CommandLineRunner {
    @Autowired
    Dao dao;
    private static Logger LOG = LoggerFactory.getLogger(Exporter.class);

    public Exporter() {
    }

    public static void main(String[] args) {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(Exporter.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    public void run(String... args) {
        LOG.info("EXECUTING : command line runner");
        long factId = Long.valueOf(args[2]);
//        long factId = 12019133;
        Company company = this.dao.getCompany();
        Date date = new Date();
        DateFormat timeFormatter = new SimpleDateFormat(Utils.TIME_FORMAT);
        String timeStr = timeFormatter.format(date);
        DateFormat dateFormatter = new SimpleDateFormat(Utils.DATE_FORMAT);
        String dateStr = dateFormatter.format(date);
        Client client = this.dao.getClient(factId);
        List<QueryN7Ent> list = this.dao.getQueryN7EntList(factId);
        QueryN7Ent datafactura = list.get(0);

        try {
            StringBuilder s = new StringBuilder();
            s.append("<?xml version=\"1.0\" encoding=\"windows-1251\" ?>");
            String idFile = String.format("k-telecom-%s-%s.xml", factId, (new Date()).getTime());
            s.append("<���� ��������=\"����3\" ��������=\"5.01\" ������=\"" + idFile + "\">");
            s.append("");
            s.append("  <����������� ������=\"\" �����=\"\">");
            s.append("  </�����������>");
            s.append("");
            s.append("  <�������� ���������=\"" + timeStr + "\" ���������=\"" + dateStr + "\" ���=\"1115131\" ���������������=\"" + company.getCompanyName() + "\" �������=\"���\">");
            s.append("    <�������� �������=\"" + convertDate(datafactura.getFactDate()) + " �� ������ � " + convertDate(datafactura.getsDate()) + " �� " + convertDate(datafactura.geteDate()) +"\" ������=\"643\" ��������=\"" + datafactura.getFactNum() + "\">");
            s.append("      <������>");
            s.append("        <����>");
            s.append("          <������ �����=\"" + company.getInn() + "\" ���=\"" + company.getTrrc() + "\" �������=\"" + company.getCompanyName() + "\"/>");
            s.append("        </����>");
            s.append("        <�����>");
            s.append("        <����� ���������=\"\" �����=\"" + company.getJurAddress() + "\"/>");
            s.append("        </�����>");
            s.append("      </������>");
            s.append("      <���������>");
            s.append("        <����>");
            s.append("          <������ �����=\"" + client.getInn() + "\" ���=\"" + client.getTrrc() + "\" �������=\"" + client.getName() + "\"/>");
            s.append("        </����>");
            s.append("      </���������>");
            s.append("      <�������>");
            s.append("        <����>");
            s.append("          <������ �����=\"" + client.getInn() + "\" ���=\"" + client.getTrrc() + "\" �������=\"" + client.getName() + "\"/>");
            s.append("        </����>");
            s.append("        <�����>");
            s.append("        <����� ���������=\"91\" �����=\"" + client.getCustAddr() + "\"/>");
            s.append("        </�����>");
            s.append("      </�������>");
            s.append("    <��������1 ��������=\"" + "�/� � " + client.getAccount() + "\"/>");
            s.append("    </��������>");
            s.append("    <����������>");
            //List<QueryN7Ent> list = this.dao.getQueryN7EntList(factId);
            int i = 0;
            double summ = 0.0D;
            double summnal = 0.0D;
            Iterator var17 = list.iterator();
            while(var17.hasNext()) {
                QueryN7Ent queryN7Ent = (QueryN7Ent)var17.next();
                summ += queryN7Ent.getSumm();
                summnal += queryN7Ent.getVat();
                s.append("      <������� ������=\"-\" �������=\"" + queryN7Ent.getDetName() + "\" �����=\"" + queryN7Ent.getTaxRate() + "\" ������=\"" + i++ + "\" �����������=\""+ (queryN7Ent.getSumm()-queryN7Ent.getVat()) +"\" ����������=\""+ queryN7Ent.getSumm()+"\">");
                s.append("        <�����>");
                s.append("          <��������>��� ������</��������>");
                s.append("        </�����>");
                s.append("        <������>");
                s.append("          <������>" + queryN7Ent.getVat() + "</������>");
                s.append("        </������>");
                s.append("      </�������>");
            }

            s.append("      <�������� ����������������=\"" + (summ-summnal) + "\" ���������������=\"" + summ + "\">");
            s.append("        <�����������>");
            s.append("          <������>" + summnal + "</������>");
            s.append("        </�����������>");
            s.append("      </��������>");
            s.append("    </����������>");
            s.append("    <��������� �������=\"2\" �������=\"����������� �����������\" ������=\"1\">");
            s.append("      <�� �����=\"������� ���������\" �����=\"" + company.getInn() + "\" �������=\"" + company.getCompanyName() + "\">");
            String buhgalter = company.getBuhgalter();
            String lastname = buhgalter.substring(0, buhgalter.indexOf(" "));
            String firstname = buhgalter.substring(buhgalter.indexOf(" ") + 1);
            s.append("        <��� ���=\"" + firstname + "\" �������=\"" + lastname + "\"/>");
            s.append("      </��>");
            s.append("    </���������>");
            s.append("  </��������>");
            s.append("");
            s.append("</����>");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(idFile), "Cp1251"));
            writer.write(s.toString());
            writer.close();
        } catch (Exception var21) {
            var21.printStackTrace();
        }

    }

    private String convertDate(String date) {
        String[] numbers = date.substring(0, 10).split("-");
        return String.format("%s.%s.%s", numbers[2], numbers[1], numbers[0]);
    }
}
